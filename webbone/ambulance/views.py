# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render

# Create your views here.
from rest_framework.authtoken.views import ObtainAuthToken
from rest_framework.authtoken.models import Token
from rest_framework.response import Response
from django.contrib.auth.models import Group,User
from django.http import JsonResponse
from rest_framework.views import APIView
from rest_framework.generics import ListAPIView,RetrieveAPIView
from ambulance.models import*
from ambulance.serializers import*
from webbone.settings import SECRET_KEY
from time import gmtime, strftime
import jwt

class ModeratorListView(ListAPIView):
	queryset = Moderator.objects.all()
	serializer_class = ModeratorSerializer

	def list(self,request):

		API_STATUS = 'SUCCESS'

		try :

			queryset = self.get_queryset()
			serializer = ModeratorSerializer(queryset,many=True)
			return Response({'status': API_STATUS, 'list': serializer.data})

		except Exception as e:
			print(e)
			API_STATUS = 'REQUEST FAILURE'
			return Response({'status': API_STATUS})


class PatientListView(ListAPIView):
	queryset = Patient.objects.all()
	serializer_class = PatientSerializer

	def list(self,request):

		API_STATUS = 'SUCCESS'

		try :

			queryset = self.get_queryset()
			serializer = PatientSerializer(queryset,many=True)
			return Response({'status': API_STATUS, 'list': serializer.data})

		except Exception as e:
			print(e)
			API_STATUS = 'REQUEST FAILURE'
			return Response({'status': API_STATUS})


class AmbulanceListView(ListAPIView):
	queryset = AmbulanceVehicle.objects.all()
	serializer_class = AmbulanceVehicleSerializer

	def list(self,request):

		API_STATUS = 'SUCCESS'

		try :

			queryset = self.get_queryset()
			serializer = AmbulanceVehicleSerializer(queryset,many=True)
			return Response({'status': API_STATUS, 'list': serializer.data})

		except Exception as e:
			print(e)
			API_STATUS = 'REQUEST FAILURE'
			return Response({'status': API_STATUS})


class BookingListView(ListAPIView):
	queryset = Booking.objects.all()
	serializer_class = BookingSerializer

	def list(self,request):

		API_STATUS = 'SUCCESS'

		try :

			queryset = self.get_queryset()
			serializer = BookingSerializer(queryset,many=True)
			return Response({'status': API_STATUS, 'list': serializer.data})

		except Exception as e:
			print(e)
			API_STATUS = 'REQUEST FAILURE'
			return Response({'status': API_STATUS})