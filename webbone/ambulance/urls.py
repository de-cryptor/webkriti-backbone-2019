from django.conf.urls import url
from django.urls import path, include
from django.contrib import admin
from django.contrib.auth.models import Group,User
from ambulance.views import*
from login.views import*

urlpatterns = [

	url(r'^api/moderators/$',ModeratorListView.as_view(),name='view-all'),
	url(r'^api/patients/',PatientListView.as_view(),name='detail'),
	url(r'^api/ambulances/$',AmbulanceListView.as_view(),name='view-all'),
	url(r'^api/bookings/$',BookingListView.as_view(),name='view-all'),
    

]