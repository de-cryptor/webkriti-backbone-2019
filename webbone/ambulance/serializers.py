from rest_framework.serializers import ModelSerializer,PrimaryKeyRelatedField
from ambulance.models import*
from rest_framework import*

class ModeratorSerializer(ModelSerializer):

	class Meta:
		model = Moderator
		fields = "__all__"

class PatientSerializer(ModelSerializer):

	class Meta:
		model = Patient
		fields = "__all__"


class AmbulanceVehicleSerializer(ModelSerializer):


	class Meta:
		model = AmbulanceVehicle
		fields = "__all__"

class BookingSerializer(ModelSerializer):

	patient = PatientSerializer(read_only=True)
	ambulance = AmbulanceVehicleSerializer(read_only=True)
	class Meta:
		model = Booking
		fields = "__all__"