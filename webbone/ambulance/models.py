from django.db import models
from datetime import date
from django.contrib.auth.models import User



# Create your models here.
class Moderator(models.Model):

	# DATABASE FIELDS
	user = models.OneToOneField(User, on_delete=models.CASCADE,blank=True,null=True)
	name = models.CharField(max_length=200)
	phone = models.CharField(max_length=20)
	email = models.EmailField(max_length=70,blank=True,unique=True)


	# META CLASS
	class Meta:
		verbose_name = 'Moderator'
		verbose_name_plural = 'Moderators'

	# TO STRING METHOD
	def __str__(self):
		return (self.name) + "-" + str(self.email)

	# SAVE METHOD
	def save(self, *args, **kwargs):
		#do_something
		super().save(*args, **kwargs)  # Call the "real" save() method.
		#do_something


# Create your models here.
class Patient(models.Model):

	# DATABASE FIELDS

	user = models.OneToOneField(User, on_delete=models.CASCADE,blank=True,null=True)
	name = models.CharField(max_length=200)
	phone = models.CharField(max_length=20)
	email = models.EmailField(max_length=70,blank=True,unique=True)
	


	# META CLASS
	class Meta:
		verbose_name = 'Patient'
		verbose_name_plural = 'Patients'

	# TO STRING METHOD
	def __str__(self):
		return (self.name) + "-" + str(self.email)

	# SAVE METHOD
	def save(self, *args, **kwargs):
		#do_something
		super().save(*args, **kwargs)  # Call the "real" save() method.
		#do_something	


# Create your models here.
class AmbulanceVehicle(models.Model):

	# DATABASE FIELDS
	ambulance_id = models.CharField(max_length=10,primary_key=True)
	name = models.CharField(max_length=200)
	model_year = models.IntegerField(default=2018)


	# META CLASS
	class Meta:
		verbose_name = 'Ambulance Vehicle'
		verbose_name_plural = 'Ambulance Vehicles'

	# TO STRING METHOD
	def __str__(self):
		return (self.ambulance_id) + "-" + (self.name) + "-" + str(self.model_year)

	# SAVE METHOD
	def save(self, *args, **kwargs):
		#do_something
		super().save(*args, **kwargs)  # Call the "real" save() method.
		#do_something


# Create your models here.
class Booking(models.Model):

	# DATABASE FIELDS
	ambulance = models.ForeignKey(AmbulanceVehicle,on_delete=models.CASCADE)
	datetime = models.DateTimeField()
	comment = models.TextField(max_length=200,null=True,blank=True)


	# META CLASS
	class Meta:
		verbose_name = 'Booking'
		verbose_name_plural = 'Bookings'

	# TO STRING METHOD
	def __str__(self):
		return  str(self.ambulance) + "-" + str(self.datetime)

	# SAVE METHOD
	def save(self, *args, **kwargs):
		#do_something
		super().save(*args, **kwargs)  # Call the "real" save() method.
		#do_something