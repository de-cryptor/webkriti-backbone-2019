# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render

# Create your views here.
from rest_framework.authtoken.views import ObtainAuthToken
from rest_framework.authtoken.models import Token
from rest_framework.response import Response
from django.contrib.auth.models import Group,User
from django.http import JsonResponse
from rest_framework.views import APIView
from rest_framework.generics import ListAPIView,RetrieveAPIView
from login.models import*
from login.authenticate import*
from webbone.settings import SECRET_KEY
from time import gmtime, strftime
import jwt

class CustomAuthToken(ObtainAuthToken):

    def post(self, request, *args, **kwargs):
    	try:
    		serializer = self.serializer_class(data=request.data,context={'request': request })
    		serializer.is_valid(raise_exception=True)

    		user = serializer.validated_data['user']
    		ip = get_client_ip(request)
    		jwt_token = jwt.encode({ 'username': user.username,'ip': ip },SECRET_KEY,algorithm = 'HS256').decode('utf-8')
    		last_login = strftime("%d-%m-%Y %H:%M:%S", gmtime())

    		group = Group.objects.get(user=user)
    		return JsonResponse({ 'status': 'SUCCESS', 'token': jwt_token, 'group': group.name, 'ip':ip, 'session_id': 'JAN-19', 'last_login':last_login })

    	except Exception as e:
    		print(str(e))
    		return Response({ 'status': 'LOGIN_FAILED' })

